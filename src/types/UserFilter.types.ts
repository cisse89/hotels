export type UserFilter = {
  stars: number;
  numOfAdults: number;
  numOfChildren: number;
};
