type Occupancy = {
  maxAdults: number;
  maxChildren: number;
  maxOverall: number;
};

export type Room = {
  id: string;
  name: string;
  shortDescription: string;
  longDescription: string;
  occupancy: Occupancy;
};
