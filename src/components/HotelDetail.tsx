import { useContext, useEffect, useState } from "react";
import guestline from "../api/guestline";
import FilterContext from "../contexts/FilterContext";
import { Hotel } from "../types/Hotel.types";
import { Room } from "../types/Room.types";
import ImageViewer from "./ImageViewer";
import RoomItem from "./RoomItem";
import StarRating from "./StarRating";

interface MyProps {
  details: Hotel;
}

const HotelDetail: React.FC<MyProps> = ({ details }) => {
  const [roomsList, setRoomsList] = useState<Array<Room>>([]);

  const filter = useContext(FilterContext);

  useEffect(() => {
    const getRoomsFromHotel = async () => {
      const ans = await guestline.get(`roomRates/OBMNG/${details.id}`);
      setRoomsList(ans.data.rooms);
    };

    getRoomsFromHotel();
  }, []);

  const filterByGuests = (room: Room) => {
    return (
      room.occupancy.maxChildren >= filter.numOfChildren &&
      room.occupancy.maxAdults >= filter.numOfAdults
    );
  };

  const filteredRooms = roomsList
    .filter(filterByGuests)
    .map((room) => <RoomItem key={room.id} room={room} />);

  return (
    <>
      {filteredRooms.length !== 0 && (
        <div className="ui segment relaxed grid">
          <div className="ui three column row">
            <div className="ui five wide column">
              <ImageViewer images={details.images} />
            </div>
            <div className="ui five wide column">
              <div className="ui header">{details.name}</div>
              <div>{details.address1}</div>
              <div>{details.address2}</div>
            </div>
            <div className="right floated right aligned column">
              <StarRating numOfStars={details.starRating}></StarRating>
            </div>
          </div>
          <div className="ui row grid">{filteredRooms}</div>
        </div>
      )}
    </>
  );
};

export default HotelDetail;
