import { render, waitFor } from "@testing-library/react";
import { UserFilter } from "../types/UserFilter.types";
import { Hotel, HotelImage } from "../types/Hotel.types";
import { Room } from "../types/Room.types";
import mockAxios from "../__mocks__/axios";
import HotelDetail from "./HotelDetail";
import { act } from "react-dom/test-utils";
import FilterContext from "../contexts/FilterContext";

const testHotel5Star: Hotel = {
  id: "hotel1",
  name: "First Hotel 5 star",
  description: "",
  address1: "",
  address2: "",
  postcode: "",
  town: "",
  country: "",
  countryCode: "",
  starRating: "5",
  images: [{ url: "test1Url" }],
};

const testRoom1: Room = {
  id: "room1",
  name: "Test Room",
  shortDescription: "",
  longDescription: "room 1 long description",
  occupancy: {
    maxAdults: 2,
    maxChildren: 1,
    maxOverall: 0,
  },
};

const testRoom2NoChildren: Room = {
  id: "room2",
  name: "No Childs Allowed",
  shortDescription: "",
  longDescription: "room 2 long description",
  occupancy: {
    maxAdults: 2,
    maxChildren: 0,
    maxOverall: 0,
  },
};

jest.mock("./ImageViewer", () => (props: { images: Array<HotelImage> }) => {
  return <div data-testid="passedImages">{props.images[0].url}</div>;
});

jest.mock("./RoomItem", () => (props: { room: Room }) => {
  return (
    <div key={props.room.id} data-testid="passedRoom">
      {props.room.longDescription}
      {props.room.name}
    </div>
  );
});

describe("HotelDetail tests", () => {
  it("should not filter out matching room", async () => {
    mockAxios.get.mockResolvedValueOnce({ data: { rooms: [testRoom1, testRoom2NoChildren] } });

    const testUserFilter: UserFilter = { stars: 5, numOfAdults: 1, numOfChildren: 0 };
    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <HotelDetail details={testHotel5Star} />
      </FilterContext.Provider>
    );

    await waitFor(() => {
      testedComponent.getByText(/First Hotel 5 star/i);
      expect(testedComponent.getAllByTestId("passedImages")).toHaveLength(1);
      expect(testedComponent.getAllByTestId("passedRoom")).toHaveLength(2);
      testedComponent.getByText(/room 2 long description/i);
      testedComponent.getByText(/No Childs Allowed/i);
      testedComponent.getByText(/room 1 long description/i);
      testedComponent.getByText(/Test Room/i);
    });
  });

  it("should filter out room with no chilren if one child set in filter", async () => {
    mockAxios.get.mockResolvedValueOnce({ data: { rooms: [testRoom1, testRoom2NoChildren] } });

    const testUserFilter: UserFilter = { stars: 5, numOfAdults: 1, numOfChildren: 1 };
    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <HotelDetail details={testHotel5Star} />
      </FilterContext.Provider>
    );

    await waitFor(() => {
      expect(testedComponent.getAllByTestId("passedRoom")).toHaveLength(1);
      expect(testedComponent.queryByText(/No Childs Allowed/i)).toBeNull();
      testedComponent.getByText(/room 1 long description/i);
      testedComponent.getByText(/Test Room/i);
    });
  });

  it("should filter out entire hotel when no rooms matching filter", async () => {
    mockAxios.get.mockResolvedValueOnce({ data: { rooms: [testRoom1, testRoom2NoChildren] } });

    let testUserFilter: UserFilter = { stars: 5, numOfAdults: 0, numOfChildren: 0 };
    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <HotelDetail details={testHotel5Star} />
      </FilterContext.Provider>
    );

    await waitFor(() => {
      expect(testedComponent.getAllByTestId("passedImages")).toHaveLength(1);
    });

    act(() => {
      testUserFilter.numOfAdults = 10;
      testedComponent.rerender(
        <FilterContext.Provider value={testUserFilter}>
          <HotelDetail details={testHotel5Star} />
        </FilterContext.Provider>
      );
    });

    await waitFor(() => {
      expect(testedComponent.queryByTestId("passedImages")).toBeNull();
      expect(testedComponent.queryByTestId("passedRoom")).toBeNull();
      expect(testedComponent.queryByText(/First Hotel 5 star/i)).toBeNull();
    });
  });
});
