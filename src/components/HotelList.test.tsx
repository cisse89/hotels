import mockAxios from "jest-mock-axios";

import { render, waitFor } from "@testing-library/react";
import HotelsList from "./HotelsList";
import { UserFilter } from "../types/UserFilter.types";
import { Hotel } from "../types/Hotel.types";
import FilterContext from "../contexts/FilterContext";

const testUserFilter: UserFilter = { stars: 2, numOfAdults: 0, numOfChildren: 0 };

const testHotel5Star: Hotel = {
  id: "hotel1",
  name: "First Hotel 5 star",
  description: "",
  address1: "",
  address2: "",
  postcode: "",
  town: "",
  country: "",
  countryCode: "",
  starRating: "5",
  images: [],
};

const testHotel3Star: Hotel = {
  id: "hotel2",
  name: "Second Hotel 3 star",
  description: "",
  address1: "",
  address2: "",
  postcode: "",
  town: "",
  country: "",
  countryCode: "",
  starRating: "3",
  images: [],
};

jest.mock("./HotelDetail", () => (props: { details: Hotel; filter: UserFilter }) => {
  return (
    <div key={props.details.id} data-testid="passedHotelDetails">
      {props.details.name}
    </div>
  );
});

describe("HotelList tests", () => {
  it("should not filter out matching hotel", async () => {
    mockAxios.get.mockResolvedValueOnce({ data: [testHotel5Star, testHotel3Star] });
    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <HotelsList />
      </FilterContext.Provider>
    );

    await waitFor(() => {
      expect(testedComponent.getAllByTestId("passedHotelDetails")).toHaveLength(2);
    });
  });

  it("should filter out 3 start hotel when 5 star filter on", async () => {
    mockAxios.get.mockResolvedValueOnce({ data: [testHotel5Star, testHotel3Star] });

    testUserFilter.stars = 5;
    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <HotelsList />
      </FilterContext.Provider>
    );
    await waitFor(() =>
      expect(testedComponent.getAllByTestId("passedHotelDetails")).toHaveLength(1)
    );
  });
});
