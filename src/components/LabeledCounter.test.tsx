import { cleanup, fireEvent, render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import LabeledCounter from "./LabeledCounter";

describe("LabeledCounter tests", () => {
  const onValueChangeMock = jest.fn();
  let testedComponent: any;

  beforeEach(() => {
    testedComponent = render(
      <LabeledCounter label="testLabel" onValueChange={onValueChangeMock} />
    );

    onValueChangeMock.mockRestore();
  });
  afterEach(() => {
    cleanup();
  });

  it("should render all elements correctly", () => {
    expect(testedComponent.getByTestId("incrementButton"));
    expect(testedComponent.getByTestId("decrementButton"));

    expect(testedComponent.getByText(/testlabel/i));
    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("0");
  });

  it("should increment and deincrement value few times correclty", () => {
    const incrementButton = testedComponent.getByTestId("incrementButton");
    const decrementButton = testedComponent.getByTestId("decrementButton");

    act(() => {
      fireEvent.click(incrementButton);
    });
    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("1");
    expect(onValueChangeMock).lastCalledWith(1);

    act(() => {
      fireEvent.click(incrementButton);
    });
    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("2");
    expect(onValueChangeMock).lastCalledWith(2);

    act(() => {
      fireEvent.click(decrementButton);
    });
    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("1");
    expect(onValueChangeMock).lastCalledWith(1);
  });

  it("should increment to maximum value 9", () => {
    const incrementButton = testedComponent.getByTestId("incrementButton");

    for (let i in [...Array(9)]) {
      act(() => {
        fireEvent.click(incrementButton);
      });
    }

    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("9");
    expect(incrementButton.className).toContain("disabled");
    expect(onValueChangeMock).toBeCalledTimes(9);

    act(() => {
      fireEvent.click(incrementButton);
    });
    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("9");
  });

  it("should not decrement when value is 0", () => {
    const decrementButton = testedComponent.getByTestId("decrementButton");

    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("0");

    act(() => {
      fireEvent.click(decrementButton);
    });

    expect(decrementButton.className).toContain("disabled");
    expect(testedComponent.getByTestId("currentValue").textContent).toEqual("0");
    expect(onValueChangeMock).not.toBeCalled();
  });
});
