import { url } from "inspector";
import { useState } from "react";
import { HotelImage } from "../types/Hotel.types";
import "./ImageViewer.css";

interface MyProps {
  images: Array<HotelImage>;
}
const ImageViewer: React.FC<MyProps> = ({ images }) => {
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const maxIndex = images.length;

  const moveLeft = () => {
    if (currentImageIndex - 1 < 0) {
      return;
    }

    setCurrentImageIndex(currentImageIndex - 1);
  };

  const moveRight = () => {
    if (currentImageIndex + 1 > maxIndex - 1) {
      return;
    }

    setCurrentImageIndex(currentImageIndex + 1);
  };

  return (
    <div>
      <i
        data-testid="leftArrowButton"
        className="left-button ui big icon angle left teal"
        onClick={moveLeft}
      ></i>
      <img
        data-testid="hotelImage"
        alt="img"
        className="ui image"
        src={images[currentImageIndex].url}
      ></img>
      <i
        data-testid="rightArrowButton"
        className="right-button ui big icon angle right teal"
        onClick={moveRight}
      ></i>
    </div>
  );
};

export default ImageViewer;
