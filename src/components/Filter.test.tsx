import { fireEvent, render, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import FilterContext from "../contexts/FilterContext";
import { UserFilter } from "../types/UserFilter.types";
import Filter from "./Filter";

describe("Filter tests", () => {
  const changeFilter = jest.fn();

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render correctly elements of filter", async () => {
    const testUserFilter: UserFilter = { stars: 5, numOfAdults: 0, numOfChildren: 0 };

    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <Filter setFilter={changeFilter} />
      </FilterContext.Provider>
    );

    testedComponent.getByText(/adults/i);
    testedComponent.getByText(/children/i);

    // 5star part
    expect(testedComponent.getByText(/adults/i).parentNode?.firstChild?.childNodes).toHaveLength(5);
  });

  it("incrementing stars should call setFilter correctly ", async () => {
    const testUserFilter: UserFilter = { stars: 1, numOfAdults: 0, numOfChildren: 0 };

    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <Filter setFilter={changeFilter} />
      </FilterContext.Provider>
    );

    const star5 = testedComponent.getByText(/adults/i).parentNode?.firstChild?.lastChild;

    act(() => {
      fireEvent.click(star5!);
    });

    expect(changeFilter).lastCalledWith({ stars: 5, numOfAdults: 0, numOfChildren: 0 });
  });

  it("incrementing adults should call setFilter correctly ", async () => {
    const testUserFilter: UserFilter = { stars: 1, numOfAdults: 0, numOfChildren: 0 };

    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <Filter setFilter={changeFilter} />
      </FilterContext.Provider>
    );

    const adultsPLusButton = testedComponent.getAllByTestId(/incrementButton/i)[0];
    act(() => {
      fireEvent.click(adultsPLusButton);
    });

    act(() => {
      fireEvent.click(adultsPLusButton);
    });

    expect(changeFilter).lastCalledWith({ stars: 1, numOfAdults: 2, numOfChildren: 0 });
  });

  it("incrementing children should call setFilter correctly ", async () => {
    const testUserFilter: UserFilter = { stars: 1, numOfAdults: 0, numOfChildren: 0 };

    const testedComponent = render(
      <FilterContext.Provider value={testUserFilter}>
        <Filter setFilter={changeFilter} />
      </FilterContext.Provider>
    );

    const childPlusbutton = testedComponent.getAllByTestId(/incrementButton/i)[1];
    act(() => {
      fireEvent.click(childPlusbutton);
    });

    act(() => {
      fireEvent.click(childPlusbutton);
    });

    expect(changeFilter).lastCalledWith({ stars: 1, numOfAdults: 0, numOfChildren: 2 });
  });
});
