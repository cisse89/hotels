import { render } from "@testing-library/react";
import { Room } from "../types/Room.types";

import RoomItem from "./RoomItem";

const testRoom: Room = {
  id: "roomTestId",
  name: "Test room name",
  shortDescription: "shortOne",
  longDescription: "longOne",
  occupancy: {
    maxAdults: 2,
    maxChildren: 1,
    maxOverall: 0,
  },
};

describe("RoomItem tests", () => {
  it("should display name, occupancy and longDescription correctly", () => {
    const testedComponent = render(<RoomItem room={testRoom} />);

    expect(testedComponent.getByText(/test room name/i));
    expect(testedComponent.getByText(/adults: 2/i));
    expect(testedComponent.getByText(/children: 1/i));

    expect(testedComponent.getByText(testRoom.longDescription));
  });
});
