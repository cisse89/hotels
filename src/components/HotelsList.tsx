import React, { useContext, useEffect, useState } from "react";
import guestline from "../api/guestline";
import FilterContext from "../contexts/FilterContext";
import { Hotel } from "../types/Hotel.types";
import HotelDetail from "./HotelDetail";

const HotelsList: React.FC<{}> = () => {
  const [hotels, setHotels] = useState<Array<Hotel>>([]);

  const conFilter = useContext(FilterContext);

  useEffect(() => {
    const getListOfHotels = async () => {
      const ans = await guestline.get("hotels", { params: { "collection-id": "OBMNG" } });
      setHotels(ans.data);
    };

    getListOfHotels();
  }, []);

  const filterByStars = (hotel: Hotel) => {
    return parseInt(hotel.starRating) >= conFilter.stars;
  };

  const renderHotels = () => {
    return hotels
      .filter(filterByStars)
      .map((hotel) => <HotelDetail key={hotel.id} details={hotel}></HotelDetail>);
  };

  return <div className="ui relaxed divided list">{renderHotels()}</div>;
};

export default HotelsList;
