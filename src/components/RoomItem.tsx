import React from "react";
import { Room } from "../types/Room.types";

interface MyProps {
  room: Room;
}

const RoomItem: React.FC<MyProps> = ({ room }) => {
  return (
    <div className="ui two column row">
      <div className="ui grid" style={{ paddingLeft: 25 }}>
        <div className="ui row middle aligned divider"></div>
        <div className="five wide column">
          <div className="ui header">{room.name}</div>
          <div>{`Adults: ${room.occupancy.maxAdults}`}</div>
          <div>{`Children: ${room.occupancy.maxChildren}`}</div>
        </div>
        <div className="eleven wide column">{room.longDescription}</div>
      </div>
    </div>
  );
};

export default RoomItem;
