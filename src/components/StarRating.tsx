import React from "react";
interface MyProps {
  numOfStars: string;
}

const MAX_STARS_NUM = 5;
const StarRating: React.FC<MyProps> = ({ numOfStars }) => {
  const stars = parseInt(numOfStars);

  return (
    <div>
      {[...Array(MAX_STARS_NUM)].map((it, ind) => {
        return (
          <i
            key={ind}
            data-testid={`star${ind + 1}`}
            className={`ui icon star ${ind < stars ? "yellow" : ""}`}
          />
        );
      })}
    </div>
  );
};

export default StarRating;
