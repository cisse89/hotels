import { useContext } from "react";
import FilterContext from "../contexts/FilterContext";
import { UserFilter } from "../types/UserFilter.types";
import LabeledCounter from "./LabeledCounter";

interface MyProps {
  setFilter: (uf: UserFilter) => void;
}

const Filter: React.FC<MyProps> = ({ setFilter }) => {
  const filter = useContext(FilterContext);
  const starArray = [...Array(5)].map((el, ind) => {
    return (
      <i
        key={ind}
        onClick={() => {
          setFilter({ ...filter, stars: ind + 1 });
        }}
        className={`ui icon star ${ind < filter.stars ? "yellow" : ""}`}
      ></i>
    );
  });

  const changeAdultsNum = (n: number) => {
    setFilter({ ...filter, numOfAdults: n });
  };

  const changeChildsNum = (n: number) => {
    setFilter({ ...filter, numOfChildren: n });
  };

  return (
    <div className="ui segment" style={{ display: "flex" }}>
      <div className="ui column">{starArray}</div>
      <LabeledCounter label="Adults" onValueChange={changeAdultsNum} />
      <LabeledCounter label="Children" onValueChange={changeChildsNum} />
    </div>
  );
};

export default Filter;
