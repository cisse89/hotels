import { useEffect, useState } from "react";

interface MyProps {
  label: string;
  onValueChange: (n: number) => void;
}

const MAX_GUESTS = 9;
const MIN_GUESTS = 0;
const LabeledCounter: React.FC<MyProps> = ({ label, onValueChange }) => {
  const [value, setValue] = useState(0);

  useEffect(() => {
    onValueChange(value);
  }, [value]);

  const increment = () => {
    if (value === MAX_GUESTS) {
      return;
    }
    setValue(value + 1);
  };

  const decrement = () => {
    if (value === MIN_GUESTS) {
      return;
    }
    setValue(value - 1);
  };

  return (
    <div className="ui item" style={{ display: "flex", paddingLeft: 10 }}>
      {label}
      <i
        className={`ui icon plus ${value === MAX_GUESTS ? "disabled" : ""}`}
        onClick={increment}
        data-testid="incrementButton"
      ></i>
      <div data-testid="currentValue">{value}</div>
      <i
        className={`ui icon minus ${value === MIN_GUESTS ? "disabled" : ""}`}
        onClick={decrement}
        data-testid="decrementButton"
      ></i>
    </div>
  );
};

export default LabeledCounter;
