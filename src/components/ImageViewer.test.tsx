import { fireEvent, render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { HotelImage } from "../types/Hotel.types";
import ImageViewer from "./ImageViewer";

const imageArray: Array<HotelImage> = [{ url: "testUrl1" }, { url: "testUrl2" }];

describe("ImageViewer tests", () => {
  it("should display first from provided images and two buttons ", () => {
    const testedComponent = render(<ImageViewer images={imageArray} />);

    expect(testedComponent.getByTestId("leftArrowButton"));
    expect(testedComponent.getByTestId("rightArrowButton"));
    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl1");
  });

  it("should display second image from provided images after right button clicked", () => {
    const testedComponent = render(<ImageViewer images={imageArray} />);

    act(() => {
      fireEvent.click(testedComponent.getByTestId("rightArrowButton"));
    });
    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl2");
  });

  it("should display first image from provided images after right and left button clicked", () => {
    const testedComponent = render(<ImageViewer images={imageArray} />);

    act(() => {
      fireEvent.click(testedComponent.getByTestId("rightArrowButton"));
    });

    act(() => {
      fireEvent.click(testedComponent.getByTestId("leftArrowButton"));
    });
    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl1");
  });

  it("should not change images when last reached and right button clicked", () => {
    const testedComponent = render(<ImageViewer images={imageArray} />);

    act(() => {
      fireEvent.click(testedComponent.getByTestId("rightArrowButton"));
    });

    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl2");

    act(() => {
      fireEvent.click(testedComponent.getByTestId("rightArrowButton"));
    });

    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl2");
  });

  it("should not change images when first displayed and left button clicked", () => {
    const testedComponent = render(<ImageViewer images={imageArray} />);

    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl1");
    act(() => {
      fireEvent.click(testedComponent.getByTestId("leftArrowButton"));
    });

    expect(testedComponent.getByTestId("hotelImage").getAttribute("src")).toEqual("testUrl1");
  });
});
