import { render } from "@testing-library/react";

import StarRating from "./StarRating";

describe("StarRating tests", () => {
  it("should render 1 yellow stars correctly", () => {
    const testedComponent = render(<StarRating numOfStars="1" />);

    expect(testedComponent.getByTestId("star1").className).toContain("yellow");
    expect(testedComponent.getByTestId("star2").className).not.toContain("yellow");
    expect(testedComponent.getByTestId("star3").className).not.toContain("yellow");
    expect(testedComponent.getByTestId("star4").className).not.toContain("yellow");
    expect(testedComponent.getByTestId("star5").className).not.toContain("yellow");
  });
  it("should render 2 yellow stars correctly", () => {
    const testedComponent = render(<StarRating numOfStars="2" />);

    expect(testedComponent.getByTestId("star1").className).toContain("yellow");
    expect(testedComponent.getByTestId("star2").className).toContain("yellow");
    expect(testedComponent.getByTestId("star3").className).not.toContain("yellow");
    expect(testedComponent.getByTestId("star4").className).not.toContain("yellow");
    expect(testedComponent.getByTestId("star5").className).not.toContain("yellow");
  });
  it("should render 3 yellow stars correctly", () => {
    const testedComponent = render(<StarRating numOfStars="3" />);

    expect(testedComponent.getByTestId("star1").className).toContain("yellow");
    expect(testedComponent.getByTestId("star2").className).toContain("yellow");
    expect(testedComponent.getByTestId("star3").className).toContain("yellow");
    expect(testedComponent.getByTestId("star4").className).not.toContain("yellow");
    expect(testedComponent.getByTestId("star5").className).not.toContain("yellow");
  });

  it("should render 4 stars correctly", () => {
    const testedComponent = render(<StarRating numOfStars="4" />);

    expect(testedComponent.getByTestId("star1").className).toContain("yellow");
    expect(testedComponent.getByTestId("star2").className).toContain("yellow");
    expect(testedComponent.getByTestId("star3").className).toContain("yellow");
    expect(testedComponent.getByTestId("star4").className).toContain("yellow");
    expect(testedComponent.getByTestId("star5").className).not.toContain("yellow");
  });

  it("should render 5 stars correctly", () => {
    const testedComponent = render(<StarRating numOfStars="5" />);

    expect(testedComponent.getByTestId("star1").className).toContain("yellow");
    expect(testedComponent.getByTestId("star2").className).toContain("yellow");
    expect(testedComponent.getByTestId("star3").className).toContain("yellow");
    expect(testedComponent.getByTestId("star4").className).toContain("yellow");
    expect(testedComponent.getByTestId("star5").className).toContain("yellow");
  });
});
