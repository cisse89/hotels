import React from "react";
import { UserFilter } from "../types/UserFilter.types";

export default React.createContext<UserFilter>({ stars: 0, numOfAdults: 0, numOfChildren: 0 });
