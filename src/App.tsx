import "./App.css";
import HotelsList from "./components/HotelsList";
import Filter from "./components/Filter";
import { useState } from "react";
import { UserFilter } from "./types/UserFilter.types";
import FilterContext from "./contexts/FilterContext";

function App() {
  const [activeFilter, setActiveFilter] = useState<UserFilter>({
    stars: 1,
    numOfAdults: 0,
    numOfChildren: 0,
  });

  return (
    <div className="ui basic segment">
      <FilterContext.Provider value={activeFilter}>
        <div
          className="ui middle aligned row"
          style={{
            backgroundColor: "red",
            height: 100,
            opacity: 0.1,
            textAlign: "center",
            marginBottom: -35,
            padding: 30,
          }}
        >
          <h3>image placeholder</h3>
        </div>
        <div className="ui horizontal divider">
          <Filter setFilter={setActiveFilter} />
        </div>
        <div className="ui container row">
          <HotelsList />
        </div>
      </FilterContext.Provider>
    </div>
  );
}

export default App;
